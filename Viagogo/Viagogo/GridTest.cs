﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Viagogo
{
    class GridTest
    {
        public static Dictionary<Location, Event> DictOfEvents = new Dictionary<Location, Event>();
        // If the program needed to support multiple events at the same location then the 'Event'  
        // value would be a collection

        //If it were a much larger world size, such as the entire world, i would ask the user to input 
        // a maximum radius (ie a maximum of 50 miles/km), and then search a database of the event/location
        // pairs. I would ask for the radius as it would greatly reduce the amount of searches, and therefore
        // time.

        public static void Main(string[] args) 
        {
            Console.WriteLine("Please Input Coordinates:");
            string line = Console.ReadLine();
            string[] Coordinates = line.Split(',');
            int userX = 0;
            int userY = 0;

            try
            {

                 userX = int.Parse(Coordinates[0]);
                 userY = int.Parse(Coordinates[1]);
            }

            catch(FormatException)
            {
                Console.WriteLine("Please use format x,y");
                return;
            }

            Location userLocation = new Location(userX, userY);

            GenerateData();


            KeyValuePair<Location, Event>[] closestLocationEvents = GetClosestLocationEvent(userLocation);

            Console.WriteLine("Closet Events to (" + line + ")");

            foreach(KeyValuePair<Location, Event> closeLocationEvent in closestLocationEvents )
            {
                decimal cheapestTicketPrice = closeLocationEvent.Value.Tickets.Min(ticket => ticket.Price);



                int distance = CalculateDistanceBetweenLocations(userLocation, closeLocationEvent.Key);

                Console.WriteLine("Event " + closeLocationEvent.Value.UNI + " - " +cheapestTicketPrice.ToString("C", new CultureInfo("en-US"))+ ", Distance " +  distance);


            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();



        }

        public static int CalculateDistanceBetweenLocations(Location location1, Location location2)
        {
            int distance = Math.Abs(location1.XAxis - location2.XAxis) + Math.Abs(location1.YAxis - location2.YAxis);
            return distance;
        }

        public static KeyValuePair<Location, Event>[] GetClosestLocationEvent(Location userLocation)
        {
            

            KeyValuePair<Location, Event>[] closestLocationEvent = DictOfEvents.OrderBy(locationEvent => CalculateDistanceBetweenLocations(userLocation, locationEvent.Key)).Take(5).ToArray();
            return closestLocationEvent;
        }

        public static void GenerateData()
        {
            DictOfEvents = new Dictionary<Location, Event>();

            Random rand = new Random();


            int numberOfEvents = rand.Next(10, 101);

            for (int x = 1; x <= numberOfEvents; x++)
            {
                
                Location location = new Location(rand.Next(-10, 11), rand.Next(-10, 11));



                IEnumerable<Location> conflictingLocations = DictOfEvents.Keys.Where(existingLocation => location.XAxis == existingLocation.XAxis && location.YAxis == existingLocation.YAxis);

                if (conflictingLocations.Any())
                {

                    continue;
                }


                int numberOfTickets = rand.Next(0, 201);

                Ticket[] tickets = new Ticket[numberOfTickets];
                    
                for (int y = 0; y < numberOfTickets; y++)
                {

                    Double doublePrice = rand.NextDouble()* (100-1) + 1;

                   


                    Decimal price = Convert.ToDecimal(doublePrice);

                    Ticket ticket = new Ticket(price);

                    tickets[y] = ticket;

                    

                }



                Event e = new Event(tickets, x);

                DictOfEvents.Add(location, e);

            }
        

        }


    }
}
