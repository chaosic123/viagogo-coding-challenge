﻿using System;
namespace Viagogo
{
    public class Ticket
    {
        public decimal Price { get; set; }

        public Ticket(decimal Price)
        {
            this.Price = Price;
        }

    }
}
