﻿using System;
namespace Viagogo
{
    public class Location
    {
        public int XAxis { get; }
        public int YAxis { get; }


        public Location(int XAxis, int YAxis)
        {
            if(-10 <= XAxis && XAxis <= 10)
            {
                this.XAxis = XAxis;
            }

            else
            {
                Console.WriteLine("Sorry you are out of bounds. We will set your location at the maximum limit");
                if(XAxis < -10)
                {
                    this.XAxis = -10;
                }

                else if(XAxis > 10)
                {
                    XAxis = 10;
                }
            }

            if (-10 <= YAxis && YAxis <= 10)
            {
                this.YAxis = YAxis;
            }

            else
            {
                Console.WriteLine("Sorry you are out of bounds. We will set your location at the maximum limit");
                if (YAxis < -10)
                {
                    this.YAxis = -10;
                }

                else if (YAxis > 10)
                {
                    YAxis = 10;
                }
            }


        }

       
    }
}
