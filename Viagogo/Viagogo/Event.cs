﻿using System;
namespace Viagogo
{
    public class Event
    {
        
        public Ticket[] Tickets { get; set; }
        public int UNI { get; set; }


        public Event( Ticket[] tickets, int UNI)
        {
            Tickets = tickets;
            this.UNI = UNI;
        }


    }
}
